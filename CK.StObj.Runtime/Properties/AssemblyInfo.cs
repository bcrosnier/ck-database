#region Proprietary License
/*----------------------------------------------------------------------------
* This file (CK.StObj.Runtime\Properties\AssemblyInfo.cs) is part of CK-Database. 
* Copyright © 2007-2014, Invenietis <http://www.invenietis.com>. All rights reserved. 
*-----------------------------------------------------------------------------*/
#endregion

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "CK.StObj.Runtime" )]
[assembly: AssemblyDescription( "Contains objects that enables configuration and hooks of a User Model." )]
[assembly: AssemblyCulture( "" )]
[assembly: ComVisible( false )]
[assembly: Guid( "de09f486-f7b3-41c8-895f-465c8c50d5ec" )]
