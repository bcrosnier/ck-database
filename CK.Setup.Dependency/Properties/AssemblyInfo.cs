#region Proprietary License
/*----------------------------------------------------------------------------
* This file (CK.Setup.Dependency\Properties\AssemblyInfo.cs) is part of CK-Database. 
* Copyright © 2007-2014, Invenietis <http://www.invenietis.com>. All rights reserved. 
*-----------------------------------------------------------------------------*/
#endregion

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "CK.Setup.Dependency" )]
[assembly: AssemblyDescription( "" )]
[assembly: AssemblyCulture( "" )]
[assembly: ComVisible( false )]
[assembly: Guid( "477accbf-34ab-45be-94e0-8abbfc83e304" )]
