#region Proprietary License
/*----------------------------------------------------------------------------
* This file (CK.SqlServer.Core\Properties\AssemblyInfo.cs) is part of CK-Database. 
* Copyright © 2007-2014, Invenietis <http://www.invenietis.com>. All rights reserved. 
*-----------------------------------------------------------------------------*/
#endregion

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "CK.SqlServer.Core" )]
[assembly: AssemblyDescription( "Basic helpers for Sql Server interactions." )]
[assembly: AssemblyCulture( "" )]
[assembly: ComVisible( false )]
[assembly: Guid( "5e378f09-43e3-4af1-980b-3e603bda6b5a" )]
