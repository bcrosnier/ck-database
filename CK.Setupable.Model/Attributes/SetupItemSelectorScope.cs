﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK.Setup
{
    public enum SetupItemSelectorScope
    {
        None,
        All,
        DirectChildren,
        Children
    }
}
