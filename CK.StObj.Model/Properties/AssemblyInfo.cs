#region Proprietary License
/*----------------------------------------------------------------------------
* This file (CK.StObj.Model\Properties\AssemblyInfo.cs) is part of CK-Database. 
* Copyright © 2007-2014, Invenietis <http://www.invenietis.com>. All rights reserved. 
*-----------------------------------------------------------------------------*/
#endregion

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "CK.StObj.Model" )]
[assembly: AssemblyDescription( "Contains attributes and basic objects that enables StObj implementation to be defined and run." )]
[assembly: ComVisible( false )]
[assembly: Guid( "c62efa4f-393d-447b-b5c5-4befed630954" )]
