#region Proprietary License
/*----------------------------------------------------------------------------
* This file (CK.StObj.Engine\Properties\AssemblyInfo.cs) is part of CK-Database. 
* Copyright © 2007-2014, Invenietis <http://www.invenietis.com>. All rights reserved. 
*-----------------------------------------------------------------------------*/
#endregion

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "CK.StObj.Engine" )]
[assembly: AssemblyDescription( "Contains the runtime that resolves the dependency graph and generates a dynamic, configured assembly that finalizes User Model." )]
[assembly: AssemblyCulture( "" )]
[assembly: ComVisible( false )]
[assembly: Guid( "2abe9457-3d13-4021-8b9c-8956e0f0bba7" )]
