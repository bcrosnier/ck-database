#region Proprietary License
/*----------------------------------------------------------------------------
* This file (CK.Deploy.Console\Properties\AssemblyInfo.cs) is part of CK-Database. 
* Copyright © 2007-2014, Invenietis <http://www.invenietis.com>. All rights reserved. 
*-----------------------------------------------------------------------------*/
#endregion

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("CK.Deploy.Console")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("c77b2c8b-25de-4fe7-8341-bc2c76b654e6")]
