#region Proprietary License
/*----------------------------------------------------------------------------
* This file (Tests\CK.SqlServer.Setup.Engine.Tests.Model\CK.Authentication.Local\Properties\AssemblyInfo.cs) is part of CK-Database. 
* Copyright © 2007-2014, Invenietis <http://www.invenietis.com>. All rights reserved. 
*-----------------------------------------------------------------------------*/
#endregion

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "CK.Authentication.Local" )]
[assembly: AssemblyDescription( "" )]

[assembly: ComVisible( false )]
[assembly: Guid( "ac7e4a02-9937-43e9-a719-b29a947d43fc" )]
