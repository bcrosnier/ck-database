﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CK.Setup;
using CK.SqlServer;
using CK.SqlServer.Setup;

namespace SqlCallDemo.ProviderDemo
{

    [SqlPackage( Schema = "Provider", ResourcePath = "Res" ), Versions( "1.0.0" )]
    public abstract partial class ProviderDemoPackage : SqlPackage
    {
        [SqlProcedureNonQuery( "sActorOnly" )]
        public abstract string ActorOnly( IActorCallContext ctx );

        [SqlProcedureNonQuery( "sActorOnly" )]
        public abstract Task<string> ActorOnlyAsync( IActorCallContext ctx );

        [SqlProcedureNonQuery( "sCultureOnly" )]
        public abstract string CultureOnly( ICultureCallContext ctx );

        [SqlProcedureNonQuery( "sCultureOnly" )]
        public abstract Task<string> CultureOnlyAsync( ICultureCallContext ctx );

        [SqlProcedureNonQuery( "sTenantOnly" )]
        public abstract string TenantOnly( ITenantCallContext ctx );

        [SqlProcedureNonQuery( "sTenantOnly" )]
        public abstract Task<string> TenantOnlyAsync( ITenantCallContext ctx );

        [SqlProcedureNonQuery( "sActorCulture" )]
        public abstract Task<string> ActorCulture( IActorCultureCallContext ctx );

        [SqlProcedureNonQuery( "sActorCulture" )]
        public abstract Task<string> ActorCultureAsync( IActorCultureCallContext ctx );

        [SqlProcedureNonQuery( "sCultureTenant" )]
        public abstract Task<string> CultureTenant( ICultureTenantCallContext ctx );

        [SqlProcedureNonQuery( "sCultureTenant" )]
        public abstract Task<string> CultureTenantAsync( ICultureTenantCallContext ctx );

        [SqlProcedureNonQuery( "sAllContexts" )]
        public abstract Task<string> AllContexts( IAllCallContext ctx );

        [SqlProcedureNonQuery( "sAllContexts" )]
        public abstract Task<string> AllContextsAsync( IAllCallContext ctx );

    }
}
