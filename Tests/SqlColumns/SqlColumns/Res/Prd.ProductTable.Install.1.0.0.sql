﻿--[beginscript]

create table Prd.tProduct 
(
	ProductId int not null identity(0,1),
	ProductName nvarchar( 255 ) collate LATIN1_GENERAL_BIN not null,
	CreationDate datetime2(0) not null constraint DF_Prd_tProduct_CreationDate default( sysUtcDatetime() ),

	constraint PK_Prd_tProduct primary key clustered( ProductId ),
	constraint UK_Prd_tProduct_ProductName unique ( ProductName )
);
--
insert into Prd.tProduct( ProductName ) values( N'' );

--[endscript]
